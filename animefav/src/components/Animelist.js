import React from 'react'

const Animelist = (props) => {

    const FavoriteComponent = props.favorisComponent
   
    return (
      <>
      {props.list.map((list,index) => 
          <div className= 'container d-flex justify-content-start m-3' >
              <p>{list.titre}</p>
              <div
              className='overlay d-flex align-items-center justify-content-center'
              onClick={() => props.handleFavouriesClick(list)}>
                  <FavoriteComponent/>
              </div>
          </div>
          )}
      </>
    );
};

export default Animelist
