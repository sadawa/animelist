import React,{useState} from 'react';
import './App.css';
import AddFavoris from './components/AddFavoris';
import Animelist from './components/Animelist'
import RemoveFavoris from './components/RemoveFavoris';


function App() {
  const [favori,setfavori] = useState ([])
  const [list] = useState ([
    {
        titre : "naruto",
        Princi :" naruto"
        
    },
    {
        titre : "one piece",
        Princi: "luffy"
        
    },
    {
        titre : "jujutsu",
        Princi : "juji"
        
    },
    {
        titre : "bleach",
        Princi : "ichigo"
        
    },
    {
        titre : "dragon ball",
        Princi : "goku"
    },
])
  const addFavori = (list) => {
    const favlist = [...favori,list];
    setfavori(favlist);
  }
  const removeFavori = (list) => {
    const favlist = favori.filter((favoris) => favoris.id !== list.id);

    setfavori(favlist);
  }

  
  return (
    <div className="Container fluid">
     <Animelist list = {list} handleFavouriesClick={addFavori} favorisComponent={AddFavoris}/>
     <div className='row d-flex align-items-center mt-4 mb-4'>
     
        </div>
        <div className="float-right">
        <Animelist
          list={favori}
					handleFavouriesClick={removeFavori}
          favorisComponent={RemoveFavoris}
          />
			</div>
    </div>
   
  );
}

export default App;
